# coding: utf-8

# Começando com os imports
import csv
import matplotlib.pyplot as plt

############################## Observações ######################################
#                                                                               #
# Foi mudado o módulo CSV para carregar os dados em uma lista de dicionários    #
# As chaves dos dicionarios sao represensadas por inteiros de 0 a 7.            #
# foram usadas essas chaves para passar nos testes das tarefas                  #
# e nao modificar o codigo dos mesmos.                                          #
#                                                                               #
# Foram eleboradas outras perguntas para explorar melhor o conjunto de dados    #
# fornecidos pelo Motivate, Essas perguntas estao no após a tarefa 12           #
#                                                                               #
#                                                                               #
#################################################################################


# Vamos ler os dados como uma lista
print("Lendo o documento...")
with open("chicago.csv", "r") as file_read:
    """ Foi Modificado para carregar no data_list uma lista de dicionarios 
        fieldnames : são chaves dos maps
        0: Start Time 
        1: End Time 
        2: Trip Duration 
        3: Start Station 
        4: End Station 
        5: User Type 
        6: Gender 
        7: Birth Year
    """
    
    reader = csv.DictReader(file_read,fieldnames=[0,1,2,3,4,5,6,7])       
    data_list = list(reader)
print("Ok!")

def showData(data_list,n):
    """Imprime no console os n primeiro registros do conjunto de amostras.
        
    INPUT:
    data_list: Uma lista de dicionários 
    n: Número de amostras a serem mostrados.
       
    """
    size_list = len(data_list)
    if size_list == 0:
        print("O arquivo não contém nenhuma amostra!")
        return        
    if(n > size_list):
        n = size_list

    for row in data_list[1:n+1]:        
       print('{0} |{1} |{2} |{3} |{4} |{5} |{6} |{7} '.format(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))

def showColumn(data_list, n, key):   
    """Imprime no console os n primeiro registros de uma coluna
       do conjunto de amostras.
        
    INPUT:
    data_list: Uma lista de dicionários 
    n: Número de amostras a serem mostrados.
    key : a chave do map   
    """  
    size_list = len(data_list)
    if size_list == 0:
        print("O arquivo não contém nehuma amostra!")
        return        
    if(n > size_list):
        n = size_list

    for line in data_list[1:n+1]:
        print(line[key])           



# Vamos verificar quantas linhas nós temos
print("Número de linhas:")
print(len(data_list))



# Imprimindo a primeira linha de data_list para verificar se funcionou.
print("Linha 0: ")
#print(data_list[0])
# Aqui foi modificado a forma que imprime o cabecalho, 
# ja que mudamos a estrutura para trabalhar com uma lista de maps e a saida ficar mais amigavel
h = data_list[0] #header
print('{0} |{1} |{2} |{3} |{4} |{5} |{6} |{7}'.format(h[0],h[1],h[2],h[3],h[4],h[5],h[6],h[7]))
# É o cabeçalho dos dados, para que possamos identificar as colunas.

# Imprimindo a segunda linha de data_list, ela deveria conter alguns dados
print("Linha 1: ")
print(data_list[1])

input("Aperte Enter para continuar...")
# TAREFA 1
# TODO: Imprima as primeiras 20 linhas usando um loop para identificar os dados.
print("\n\nTAREFA 1: Imprimindo as primeiras 20 amostras")
showData(data_list,20)

# Vamos mudar o data_list para remover o cabeçalho dele.
data_list = data_list[1:]

# Nós podemos acessar as features pelo índice
# Por exemplo: sample[6] para imprimir gênero, ou sample[-2]

input("Aperte Enter para continuar...")
# TAREFA 2
# TODO: Imprima o `gênero` das primeiras 20 linhas

print("\nTAREFA 2: Imprimindo o gênero das primeiras 20 amostras")
showColumn(data_list,20,6)

# Ótimo! Nós podemos pegar as linhas(samples) iterando com um for, e as colunas(features) por índices.
# Mas ainda é difícil pegar uma coluna em uma lista. Exemplo: Lista com todos os gêneros

input("Aperte Enter para continuar...")
# TAREFA 3
# TODO: Crie uma função para adicionar as colunas(features) de uma lista em outra lista, na mesma ordem
def column_to_list(data, index):    
    column_list = []    
    if(len(data) <=0):
        return column_list
    # Dica: Você pode usar um for para iterar sobre as amostras, pegar a feature pelo seu índice, e dar append para uma lista
    # ajusta a key do map
    key = index    
    if(index < 0):
        key = 8 + index
    # itera e adiciona valor da coluna na lista      
    for row in data:
        column_list.append(row[key])
    return column_list


# Vamos checar com os gêneros se isso está funcionando (apenas para os primeiros 20)
print("\nTAREFA 3: Imprimindo a lista de gêneros das primeiras 20 amostras")
print(column_to_list(data_list, -2)[:20])

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert type(column_to_list(data_list, -2)) is list, "TAREFA 3: Tipo incorreto retornado. Deveria ser uma lista."
assert len(column_to_list(data_list, -2)) == 1551505, "TAREFA 3: Tamanho incorreto retornado."
assert column_to_list(data_list, -2)[0] == "" and column_to_list(data_list, -2)[1] == "Male", "TAREFA 3: A lista não coincide."
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# Agora sabemos como acessar as features, vamos contar quantos Male (Masculinos) e Female (Femininos) o dataset tem
# TAREFA 4
# TODO: Conte cada gênero. Você não deveria usar uma função para isso.
male = 0
female = 0
for genre in column_to_list(data_list, -2):
    if genre == 'Male':
        male +=1
    if genre == 'Female':
        female +=1    

# Verificando o resultado
print("\nTAREFA 4: Imprimindo quantos masculinos e femininos nós encontramos")
print("Masculinos: ", male, "\nFemininos: ", female)

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert male == 935854 and female == 298784, "TAREFA 4: A conta não bate."
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# Por que nós não criamos uma função para isso?
# TAREFA 5
# TODO: Crie uma função para contar os gêneros. Retorne uma lista.
# Isso deveria retornar uma lista com [count_male, count_female] (exemplo: [10, 15] significa 10 Masculinos, 15 Femininos)
def count_gender(data_list):
    male = 0
    female = 0
    # usa a funcao column-to_list para pegar a lista de generos do conjunto de amostras
    gender_list = column_to_list(data_list, -2)
    male = gender_list.count('Male')
    female = gender_list.count('Female')
    return [male, female]


print("\nTAREFA 5: Imprimindo o resultado de count_gender")
print(count_gender(data_list))


# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert type(count_gender(data_list)) is list, "TAREFA 5: Tipo incorreto retornado. Deveria retornar uma lista."
assert len(count_gender(data_list)) == 2, "TAREFA 5: Tamanho incorreto retornado."
assert count_gender(data_list)[0] == 935854 and count_gender(data_list)[1] == 298784, "TAREFA 5: Resultado incorreto no retorno!"
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# Agora que nós podemos contar os usuários, qual gênero é mais prevalente?
# TAREFA 6
# TODO: Crie uma função que pegue o gênero mais popular, e retorne este gênero como uma string.
# Esperamos ver "Male", "Female", ou "Equal" como resposta.
def most_popular_gender(data_list):
    answer = ""
    # pega a quantidade de cada genero para fazer a comparação
    quantity_gender =  count_gender(data_list)
    if  quantity_gender[0] > quantity_gender[1]:
        answer = "Male"
    else:
        answer = "Female"    
    return answer


print("\nTAREFA 6: Qual é o gênero mais popular na lista?")
print("O gênero mais popular na lista é: ", most_popular_gender(data_list))

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert type(most_popular_gender(data_list)) is str, "TAREFA 6: Tipo incorreto no retorno. Deveria retornar uma string."
assert most_popular_gender(data_list) == "Male", "TAREFA 6: Resultado de retorno incorreto!"
# -----------------------------------------------------

# Se tudo está rodando como esperado, verifique este gráfico!
gender_list = column_to_list(data_list, -2)
types = ["Male", "Female"]
quantity = count_gender(data_list)
y_pos = list(range(len(types)))
plt.bar(y_pos, quantity)
plt.ylabel('Quantidade')
plt.xlabel('Gênero')
plt.xticks(y_pos, types)
plt.title('Quantidade por Gênero')
plt.show(block=True)

input("Aperte Enter para continuar...")
# TAREFA 7
# TODO: Crie um gráfico similar para user_types. Tenha certeza que a legenda está correta.
print("\nTAREFA 7: Verifique o gráfico!")

def count_types(data_list):
    custumer = 0
    subscriber = 0
    types_list = column_to_list(data_list, 5)
    custumer = types_list.count('Customer')
    subscriber = types_list.count('Subscriber')
    return [custumer, subscriber]

types_list = column_to_list(data_list, 5)
types = ["Customer", "Subscriber"]
quantity = count_types(data_list)
y_pos = list(range(len(types)))
plt.bar(y_pos, quantity)
plt.ylabel('Quantidade')
plt.xlabel('Tipo de Usuário')
plt.xticks(y_pos, types)
plt.title('Quantidade por Tipo de Usuário')
plt.show(block=True)

input("Aperte Enter para continuar...")
# TAREFA 8
# TODO: Responda a seguinte questão
male, female = count_gender(data_list)
print("\nTAREFA 8: Por que a condição a seguir é Falsa?")
print("male + female == len(data_list):", male + female == len(data_list))
answer = "Escreva sua resposta aqui.."
print("resposta:", answer)

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert answer != "Escreva sua resposta aqui.", "TAREFA 8: Escreva sua própria resposta!"
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# Vamos trabalhar com trip_duration (duração da viagem) agora. Não conseguimos tirar alguns valores dele.
# TAREFA 9
# TODO: Ache a duração de viagem Mínima, Máxima, Média, e Mediana.
# Você não deve usar funções prontas para isso, como max() e min().
trip_duration_list = column_to_list(data_list, 2)
min_trip = 0.
max_trip = 0.
mean_trip = 0.
median_trip = 0.
########### Inicio do calculo ###############
soma_duracao_viagem = 0
total_registros = 0
sorted_trip_duration_list =[]
#############################################
for trip_duration in trip_duration_list:
    if trip_duration != "" and min_trip == 0:
        min_trip = int(trip_duration)
    if trip_duration != "":
        sorted_trip_duration_list.append(int(trip_duration))
        total_registros+=1
        soma_duracao_viagem+=int(trip_duration)
        if int(trip_duration) > max_trip:
            max_trip = int(trip_duration)
        if int(trip_duration) < min_trip:
            min_trip = int(trip_duration)  
##############################################                  

############ Calculando a Média ################
mean_trip = soma_duracao_viagem/total_registros
##############################################

#Calculando a mediana
sorted_trip_duration_list.sort()
len_list = len(sorted_trip_duration_list)
index = len_list // 2
if len_list  % 2 == 1:
    median_trip = sorted_trip_duration_list[index] 
else:
    median_trip = (sorted_trip_duration_list[index-1]  + sorted_trip_duration_list[index])/2 

print("\nTAREFA 9: Imprimindo o mínimo, máximo, média, e mediana")
print("Min: ", min_trip, "Max: ", max_trip, "Média: ", mean_trip, "Mediana: ", median_trip)

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert round(min_trip) == 60, "TAREFA 9: min_trip com resultado errado!"
assert round(max_trip) == 86338, "TAREFA 9: max_trip com resultado errado!"
assert round(mean_trip) == 940, "TAREFA 9: mean_trip com resultado errado!"
assert round(median_trip) == 670, "TAREFA 9: median_trip com resultado errado!"
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# TAREFA 10
# Gênero é fácil porque nós temos apenas algumas opções. E quanto a start_stations? Quantas opções ele tem?
# TODO: Verifique quantos tipos de start_stations nós temos, usando set()
start_stations = set(column_to_list(data_list, 3))

print("\nTAREFA 10: Imprimindo as start stations:")
print(len(start_stations))
print(start_stations)

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert len(start_stations) == 582, "TAREFA 10: Comprimento errado de start stations."
# -----------------------------------------------------

input("Aperte Enter para continuar...")
# TAREFA 11
# Volte e tenha certeza que você documentou suas funções. Explique os parâmetros de entrada, a saída, e o que a função faz. Exemplo:
# def new_function(param1: int, param2: str) -> list:
"""  Função de exemplo com anotações
            Argumentos:
                param1: O primeiro parâmetro.
                param2: O segundo parâmetro.
            Retorna:
                Uma lista de valores x.
"""
input("Aperte Enter para continuar...")
# TAREFA 12 - Desafio! (Opcional)
# TODO: Crie uma função para contar tipos de usuários, sem definir os tipos
# para que nós possamos usar essa função com outra categoria de dados.
print("Você vai encarar o desafio? (yes ou no)")
answer = "yes"

def count_items(column_list):
    item_types = []
    count_items = []
    for item in column_list:
        if item not in item_types:
            item_types.append(item)
            count_items.append(column_list.count(item))
    return item_types, count_items


if answer == "yes":
    # ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
    column_list = column_to_list(data_list, -2)
    types, counts = count_items(column_list)
    print("\nTAREFA 12: Imprimindo resultados para count_items()")
    print("Tipos:", types, "Counts:", counts)
    assert len(types) == 3, "TAREFA 12: Há 3 tipos de gênero!"
    assert sum(counts) == 1551505, "TAREFA 12: Resultado de retorno incorreto!"
    # -----------------------------------------------------
