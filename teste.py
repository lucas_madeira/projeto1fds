# coding: utf-8

# Começando com os imports
import csv
import matplotlib.pyplot as plt

with open("chicago.csv", "r") as file_read:
    """ Foi Modificado para carregar no data_list uma lista de dicionarios 
        fieldnames : são chaves dos maps
        0: Start Time 
        1: End Time 
        2: Trip Duration 
        3: Start Station 
        4: End Station 
        5: User Type 
        6: Gender 
        7: Birth Year
    """
    
    reader = csv.DictReader(file_read,fieldnames=[0,1,2,3,4,5,6,7])       
    data_list = list(reader)
print("Ok!")

numero_start_station = 0
numero_end_station = 0
stations = []
for dicionario in data_list:
    if dicionario[3] != '' and dicionario[6] == '':        
        numero_start_station += 1
        if dicionario[3] not in stations:
            stations.append(dicionario[3])
    if dicionario[4] != '' and dicionario[6] == '':        
        numero_end_station += 1
        if dicionario[4] not in stations:
            stations.append(dicionario[4])



#print(numero_start_station)
#print(numero_end_station)        
#print(stations)